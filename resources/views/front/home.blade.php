@extends('front.index')

@section('content')
    <div class="container clearfix">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 bottommargin">
                <div class="col_full bottommargin-lg clearfix">
                    @foreach ($posts as $post)
                        <div class="ipost clearfix">
                            <div class="col_half bottommargin-sm">
                                <div class="entry-image"><a href="{{ url('news/' . $post->slug) }}" alt=""><img src="{{ url('uploads/posts/medium/' . $post->banner) }}" alt=""></a></div>
                            </div>
                            <div class="col_half bottommargin-sm col_last">
                                <div class="entry-title">
                                    <h3><a href="{{ url('news/' . $post->slug) }}">{{ $post->title }}</a></h3>
                                </div>
                                <ul class="entry-meta clearfix">
                                    <li><i class="icon-calendar3"></i> {{ $post->publishdate }}</li>
                                </ul>
                                <div class="entry-content">
                                    <p>{!! strip_tags(substr($post->content, 0, 250)) !!}...</p>
                                </div>
                            </div>
                        </div>
                    @endforeach 
                </div>
            </div>

            <div class="col-md-4">
                <!-- Insert Sidebar -->
                @include('front.sidebar')
            </div>
        </div>
    </div>
@endsection