@extends('front.index')

@section('content')
    <section id="page-title" class="page-title-mini" style="margin-top:-80px;">
        <div class="container clearfix">
            <ol class="breadcrumb">
                <li><a href="{{ url('/') }}">Beranda</a></li>
                <li><a href="{{ url('/category/' . $category->slug) }}">{{ $category->title }}</a></li>
            </ol>
        </div>
    </section>

    <section id="content">
        <div class="content-wrap">
            <div class="container clearfix">
                <div class="col-md-8 nobottommargin clearfix">
                    <div id="posts" class="small-thumbs">
                        @foreach ($posts as $post)
                            <div class="entry clearfix">
                                <div class="entry-image">
                                    <a href="{{ url('/news/' . $post->slug) }}" data-lightbox="image"><img class="image_fade" src="{{ url('uploads/posts/medium/' . $post->banner) }}" alt="{{ $post->title }}"></a>
                                </div>
                                <div class="entry-c">
                                    <div class="entry-title">
                                        <h2><a href="{{ url('/news/' . $post->slug) }}">{{ $post->title }}</a></h2>
                                    </div>
                                    <ul class="entry-meta clearfix">
                                        <li><i class="icon-calendar3"></i> {{ $post->publishdate }}</li>
                                    </ul>
                                    <div class="entry-content">
                                        <p>{!! strip_tags(substr($post->content, 0, 200)) !!}...</p>
                                        <a href="{{ url('/news/' . $post->slug) }}" class="more-link">Read More</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach 
                    </div>
                    <div class="col-md-12 text-center" style="margin-top:30px;">
                        {{ $posts->links() }}
                    </div>
                </div>
                <div class="col-md-4 nobottommargin clearfix">
                    <!-- Insert Sidebar -->
                    @include('front.sidebar')
                </div>
            </div>
        </div>
    </section>
@endsection