<div id="top-bar">
	<div class="container clearfix">
		<div class="col_half nobottommargin">
			<div class="top-links">
				<ul>
					<li><a href="#">About</a></li>
					<li><a href="#">Contact</a></li>
					<li><a href="javascript:void(0)">Language</a>
						<div class="top-link-section">
							<form method="post" action="#" role="form" style="margin-bottom:0px;">
								<input type="hidden" name="refer" value="" />
								<div class="form-group">
									<select class="form-control" name="lang" required="">
                                        <option value="">English</option>
									</select>
								</div>
								<button class="btn btn-danger btn-block" type="submit">Select Language</button>
							</form>
						</div>
					</li>
				</ul>
			</div>
		</div>

		<div class="col_half fright col_last nobottommargin">
			<div id="top-social">
				<ul>
					<li><a href="javascript:void(0)" class="si-facebook"><span class="ts-icon"><i class="icon-facebook"></i></span><span class="ts-text">Facebook</span></a></li>
					<li><a href="javascript:void(0)" class="si-twitter"><span class="ts-icon"><i class="icon-twitter"></i></span><span class="ts-text">Twitter</span></a></li>
					<li><a href="javascript:void(0)" class="si-instagram"><span class="ts-icon"><i class="icon-instagram2"></i></span><span class="ts-text">Instagram</span></a></li>
					<li><a href="tel:+62 000 0000 0000" class="si-call"><span class="ts-icon"><i class="icon-call"></i></span><span class="ts-text">+62 000 0000 0000</span></a></li>
					<li><a href="mailto:" class="si-email3"><span class="ts-icon"><i class="icon-email3"></i></span><span class="ts-text">admin@gmail.com</span></a></li>
				</ul>
			</div>
		</div>
	</div>
</div>

<header id="header" class="sticky-style-2">
	<div class="container clearfix">
		<div id="logo">
			<a href="#" class="standard-logo" data-dark-logo="{{ url('/front/images/logo.png') }}"><img src="{{ url('/front/images/logo.png') }}" alt="Logo" /></a>
			<a href="#" class="retina-logo" data-dark-logo="{{ url('/front/images/logo.png') }}"><img src="{{ url('/front/images/logo.png') }}" alt="Logo" /></a>
		</div>
		<div class="top-advert">
			<img src="{{ url('/front/images/ad-long.jpg') }}" alt="">
		</div>
	</div>

	<div id="header-wrap">
		<nav id="primary-menu" class="style-2">
			<div class="container clearfix">
				<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
				<ul>
                    <li><a href="{{ route('home') }}">Home</a></li>
                    @foreach ($categories as $category)
                        <li><a href="{{ url('category/' . $category->slug) }}">{{ $category->title }}</a></li>
                    @endforeach 
                </ul>
				<div id="top-search">
					<a href="javascript:void(0)" id="top-search-trigger"><i class="icon-search3"></i><i class="icon-line-cross"></i></a>
					<form action="#" method="post">
						<input type="text" name="search" class="form-control" value="" placeholder="Search...">
					</form>
				</div>
			</div>
		</nav>
	</div>
</header>