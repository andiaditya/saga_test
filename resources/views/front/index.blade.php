<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<!-- Your Basic Site Informations -->
	<title>{{ __($page['title']) }}</title>
    <meta charset="utf-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="robots" content="index, follow" />
    <meta name="description" content="{{ __($page['desc']) }}" />
    <meta name="keywords" content="{{ __($page['keyword']) }}" />
    <meta http-equiv="imagetoolbar" content="no" />
    <meta name="language" content="Indonesia" />
    <meta name="revisit-after" content="7" />
    <meta name="webcrawlers" content="all" />
    <meta name="rating" content="general" />
    <meta name="spiders" content="all" />

    <!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- Stylesheets -->
	<link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="{{ asset('/front/css/bootstrap.css') }}" type="text/css" />
	<link rel="stylesheet" href="{{ asset('/front/css/style.css') }}" type="text/css" />
	<link rel="stylesheet" href="{{ asset('/front/css/dark.css') }}" type="text/css" />
	<link rel="stylesheet" href="{{ asset('/front/css/font-icons.css') }}" type="text/css" />
	<link rel="stylesheet" href="{{ asset('/front/css/animate.css') }}" type="text/css" />
	<link rel="stylesheet" href="{{ asset('/front/css/magnific-popup.css') }}" type="text/css" />
	<link rel="stylesheet" href="{{ asset('/front/css/responsive.css') }}" type="text/css" />

	<!-- Javascript -->
	<script src="https://www.google.com/recaptcha/api.js"></script>
	<script type="text/javascript" src="{{ asset('/front/js/jquery.js') }}"></script>
	<script type="text/javascript" src="{{ asset('/front/js/plugins.js') }}"></script>
</head>
<body class="stretched no-transition">
	<div id="wrapper" class="clearfix">
		<!-- Insert Header -->
		@include('front.header')

		<section id="content">
			<div class="content-wrap">
				<!-- Insert Content -->
				@yield('content')
			</div>
		</section>

		<!-- Insert Footer -->
		@include('front.footer')
	</div>

	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- Javascript -->
	<script type="text/javascript" src="{{ asset('/front/js/functions.js') }}"></script>
</body>
</html>