@extends('front.index')

@section('content')
    <section id="content">
        <div class="content-wrap">
            <div class="container clearfix">
                <div class="col-md-8 nobottommargin clearfix">
                    <div class="single-post nobottommargin">
                        <div class="entry clearfix">
                            <div class="entry-title"><h2>{{ $post->title }}</h2></div>

                            <ul class="entry-meta clearfix">
                                <li><i class="icon-calendar3"></i> {{ date('d F Y', strtotime($post->publishdate)) }}</li>
                            </ul>

                            <div class="entry-image">
                                <a href="{{ url('uploads/posts/' . $post->banner) }}"><img src="{{ url('uploads/posts/' . $post->banner) }}" alt="{{ $post->title }}"></a>
                            </div>

                            <div class="entry-content notopmargin">
                                {!! $post->content !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 nobottommargin clearfix">
                    <!-- Insert Sidebar -->
                    @include('front.sidebar')
                </div>
            </div>
        </div>
    </section>
@endsection