<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="imagetoolbar" content="no" />
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0" />
    <!--[if gt IE 8]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <![endif]-->
    <link rel="shortcut icon" href="{{ asset('admin/images/favicon.png') }}" />
    <!-- Stylesheet -->
    <link type="text/css" rel="stylesheet" href="{{ asset('admin/css/bootstrap.min.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('admin/css/font-awesome.min.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('admin/css/login.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('admin/css/patternlock.css') }}" />
    <!-- JQuery -->
    <script type="text/javascript" src="{{ asset('admin/js/jquery/jquery-2.1.4.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/js/bootstrap/bootstrap.min.js') }}"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    @stack('css')

</head>
<body>
    <section id="login">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-wrap">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <img src="{{ asset('/front/images/logo.png') }}" class="logo" width="100" />
                            </div>
                        </div>
                        
                        @yield('content')

                        <div class="col-md-12 text-center" id="footer">
                            <p>Copyright &copy; 2019 - Web Test </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script type="text/javascript" src="{{ asset('admin/js/patternlock/patternLock.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/js/login-core.js') }}"></script>

    @stack('js')

</body>
</html>