@extends('admin.auth.layout')

@section('title', 'Login Administrator')

@section('content')

@if($errors->any())
    <div class="alert alert-danger">
        {{ $errors->first() }}
    </div>
@else 
    <div class="alert alert-info">
        Silahkan isi Email dan Password
    </div>
@endif

<form role="form" action="{{ route('admin.login.process') }}" method="post" id="form-login" autocomplete="off">
    @csrf     
    <div class="form-group">
        <label for="login-username">Email</label>
        <input type="email" name="email" id="login-username" class="form-control" placeholder="Email">
    </div>
    <div class="form-group">
        <label for="login-password">Password</label>
        <input type="password" name="password" id="login-password" class="form-control" placeholder="Password">
    </div>
    <div class="form-group form-actions box-action">
        <button id="btn-login" class="btn btn-block btn-success"> Login </button>
    </div>
    <div class="form-group">
        <label for="">Or Login With : </label>
        <div class="row">
            <div class="col-md-12 offset-md-12">
                <a href="{{ url('/socmed/google') }}" class="btn btn-danger"><i class="fa fa-github"></i> Google</a>
                <a href="{{ url('/socmed/twitter') }}" class="btn btn-info"><i class="fa fa-twitter"></i> Twitter</a>
                <a href="{{ url('/socmed/facebook') }}" class="btn btn-primary"><i class="fa fa-facebook"></i> Facebook</a>
            </div>
        </div>
    </div>
</form>

@endsection