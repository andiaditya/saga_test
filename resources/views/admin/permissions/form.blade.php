@extends('admin.layout.app')

@section('content')
    <div class="block-content">
        <form action="{{ route($form['_action']) }}" method="{{ __($form['_method']) }}">
            @csrf 
            @if (isset($data))
                @method('PUT')
                <input type="hidden" name="id" value="{{ $data->id }}">
            @endif
            <div class="row">
                <div class="col-md-12">
                    <div class="block-header">
                        <h3>{{ __($title) }}</h3>
                        <ol class="list-inline list-unstyled">
                            <li><a href="{{ route('admin.dashboard') }}">Beranda</a></li>
                            <li>/</li>
                            <li class="active">{{ __($title) }}</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="slug">Slug <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="slug" name="slug" value="{{ isset($data) ? $data->slug : '' }}" placeholder="" required="">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="title">Title <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="title" name="title" value="{{ isset($data) ? $data->title : '' }}" placeholder="" required="">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="http_method">HTTP Method <span class="text-danger">*</span></label>
                        <input type="http_method" class="form-control" id="http_method" name="http_method" value="{{ isset($data) ? $data->http_method : '' }}" placeholder="" required="">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="http_path">HTTP Path <span class="text-danger">*</span></label>
                        <input type="http_path" class="form-control" id="http_path" name="http_path" value="{{ isset($data) ? $data->http_path : '' }}" placeholder="" required="">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Submit</button>
                        <button type="reset" class="btn btn-danger pull-right" onclick="self.history.back()"><i class="fa fa-times"></i> Cancel</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection