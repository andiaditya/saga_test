@extends('admin.layout.app')

@section('content')
    <div class="block-content">
        <div class="row">
            <div class="col-md-12">
                <div class="block-header">
                    <h3>{{ __($title) }}</h3>
                    <ol class="list-inline list-unstyled">
                        <li><a href="{{ route('admin.dashboard') }}">Beranda</a></li>
                        <li>/</li>
                        <li class="active">{{ __($title) }}</li>
                    </ol>
                </div>
            </div>
        </div>
        @if (session('message'))
            <div class="alert alert-success">
                {{ session('message') }}
            </div>
        @endif
        <div class="row">
            <div class="col-lg-12">
                <form action="{{ route('admin.permission.delete-multiple') }}" class="form-multiple-delete" method="POST">
                    @csrf 
                    @method('DELETE')
                    <table class="table table-bordered table-striped table-hover" id="datatable">   
                        <thead>
                            <tr>
                                <th class="row-selected"></th>
                                <th>Slug</th>
                                <th>Name</th>
                                <th>HTTP Method</th>
                                <th>HTTP Path</th>
                                <th class="row-action">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>
                                    <input type="checkbox" name="checkAll" id="checkAll">
                                </th>
                                <th colspan="5">
                                    <button type="submit" class="btn btn-sm btn-danger">
                                        <i class="fa fa-fw fa-trash-o"></i> Delete Selected Row
                                    </button>
                                </th>
                            </tr>
                        </tfoot>
                    </table>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script type="text/javascript">
        $('#datatable').dataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('admin.permission.json-tables') }}",
            order: [[ 1, "desc" ]],
            columns: [
                { data: 'select', name: 'select', orderable: false },
                { data: 'slug', name: 'slug' }, 
                { data: 'title', name: 'title' },
                { data: 'http_method', name: 'http_method' },
                { data: 'http_path', name: 'http_path' },
                { data: 'action', name: 'action', orderable: false }
            ]
        }); 
    </script>
@endpush