@extends('admin.layout.app')

@section('content')
    <div class="block-content">
        <form action="{{ route($form['_action']) }}" method="{{ __($form['_method']) }}" enctype="multipart/form-data">
            @csrf 
            @if (isset($data))
                @method('PUT')
                <input type="hidden" name="id" value="{{ $data->id }}">
            @endif
            <div class="row">
                <div class="col-md-12">
                    <div class="block-header">
                        <h3>{{ __($title) }}</h3>
                        <ol class="list-inline list-unstyled">
                            <li><a href="{{ route('admin.dashboard') }}">Beranda</a></li>
                            <li>/</li>
                            <li class="active">{{ __($title) }}</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <label for="title">Title <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="title" name="title" value="{{ isset($data) ? $data->title : '' }}" required>
                    </div>
                    <div class="form-group">
                        <label for="title">Content <span class="text-danger">*</span></label>
                        <div class="row" style="margin-top:-30px;">
                            <div class="col-md-12">
                                <div class="pull-right">
                                    <div class="input-group">
                                        <span class="btn-group">
                                            <a class="btn btn-sm btn-default tiny-visual" data-lang="1">Visual</a>
                                            <a class="btn btn-sm btn-success tiny-text" data-lang="1">Text</a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <textarea class="form-control" name="content" id="po-wysiwyg" rows="20">
                            {{ isset($data) ? $data->content : '' }}
                        </textarea>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="slug">SEO Title <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="slug" name="slug" value="{{ isset($data) ? $data->slug : '' }}" required>
                        <span class="help-block">Permalink : {{ url('/') }}/news/<span id="permalink"></span></span>
                    </div>
                    <div class="form-group">
                        <label for="id_category">Category <span class="text-danger">*</span></label>
                        <div class="box-category">
                            <ul class="list-unstyled">
                                @foreach ($category as $x)  
                                    <li><input type="checkbox" name="categories[]" value="{{ $x->id }}" style="margin-left:0px;" {{ is_null($x->selected) ? '' : 'checked' }}> {{ $x->title }}</li>
                                @endforeach 
                            </ul>
                        </div>
                        <div class="category-option">
                            <div class="pull-left"><a href="{{ route('admin.category.create') }}" target="_blank"><i class="fa fa-plus"></i> Add New Category</a></div>
                            <div class="pull-right"><a href="javascript:void(0)" id="category-refresh" data-id="0"><i class="fa fa-refresh"></i> Refresh</a></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="banner">Banner</label>
                        <div style="position:relative;">
                            <a class='btn btn-primary' href='javascript:;'>
                                Choose File...
                                <input type="file" style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' name="banner" size="40"  onchange='$("#upload-file-info").html($(this).val());'>
                            </a>
                            &nbsp;
                            <span class='label label-info' id="upload-file-info"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="publishdate">Publish Date <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="publishdate" name="publishdate" value="{{ isset($data) ? $data->publishdate : date('Y-m-d') }}" placeholder="" required="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Submit</button>
                        <button type="reset" class="btn btn-danger pull-right" onclick="self.history.back()"><i class="fa fa-times"></i> Cancel</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@push('js')
    <script type="text/javascript">
        tinymce.init({
            selector: "#po-wysiwyg",
            editor_deselector : "mceNoEditor",
            skin: "lightgray",
            content_css : "http://balitbangda.papuabaratprov.go.id/po-includes/css/bootstrap.min.css,http://balitbangda.papuabaratprov.go.id/po-includes/css/font-awesome.min.css",
            plugins: [
                "advlist autolink link image lists charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
                "table contextmenu directionality emoticons paste textcolor responsivefilemanager",
                "code fullscreen youtube autoresize codemirror codesample"
            ],
            menubar : false,
            toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist | outdent indent table",
            toolbar2: "| styleselect | link unlink anchor | responsivefilemanager image media youtube | forecolor backcolor | code codesample fullscreen ",
            image_advtab: true,
            fontsize_formats: "8px 10px 12px 14px 18px 24px 36px",
            relative_urls: false,
            remove_script_host: false,
            external_filemanager_path: "../po-includes/js/filemanager/",
            filemanager_title: "File Manager",
            external_plugins: {
                "filemanager" : "http://balitbangda.papuabaratprov.go.id/po-includes/js/filemanager/plugin.min.js"
            },
            codemirror: {
                indentOnInit: true,
                path: "http://balitbangda.papuabaratprov.go.id/po-includes/js/codemirror"
            }
        });

        $('#title').on('input', function() {
            var permalink;
            permalink = $.trim($(this).val());
            permalink = permalink.replace(/\s+/g,' ');
            $('#slug').val(permalink.toLowerCase());
            $('#slug').val($('#slug').val().replace(/\W/g, ' '));
            $('#slug').val($.trim($('#slug').val()));
            $('#slug').val($('#slug').val().replace(/\s+/g, '-'));
            var gappermalink = $('#slug').val();
            $('#permalink').html(gappermalink);
        });

        $('.tiny-text').on('click', function (e) {
            e.stopPropagation();
            tinymce.EditorManager.execCommand('mceRemoveEditor',true, 'po-wysiwyg');
        });

        $('.tiny-visual').on('click', function (e) {
            e.stopPropagation();
            tinymce.EditorManager.execCommand('mceAddEditor',true, 'po-wysiwyg');
        });

        $('#category-refresh').on('click', function(){
            $('.box-category').html('<div class="category-load text-success"><i class="fa fa-refresh"></i> Loading...</div>');
            $.ajax({
                type: "GET",
                url: "{{ route('admin.category.json-select') }}",
                cache: false,
                success: function(data){
                    $('.box-category').html(data);
                }
            });
            return false;
        });

        $('#publishdate').datetimepicker({
            format: 'YYYY-MM-DD',
            showTodayButton: true,
            showClear: true
        }); 

    </script>
@endpush