<div class="text-center">
    <div class="btn-group btn-group-xs">
        @foreach ($btn as $idx => $x)
            <a href="{{ $x['route'] }}" data-toggle="tooltip" data-placement="top" title="Tooltip on top" class="btn btn-xs {{ $x['btn'] }}" {{ isset($x['custom_attr']) ? $x['custom_attr'] : '' }}>
                <i class="fa fa-fw {{ $x['icon'] }}"></i>
            </a>
        @endforeach 
    </div>
</div>