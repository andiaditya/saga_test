<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="imagetoolbar" content="no" />
        <meta http-equiv="Copyright" content="" />
        <meta name="robots" content="index, follow" />
        <meta name="description" content="Administrator" />
        <meta name="generator" content="" />
        <meta name="author" content="Andi Aditya" />
        <meta name="language" content="Indonesia" />
        <meta name="revisit-after" content="7" />
        <meta name="webcrawlers" content="all" />
        <meta name="rating" content="general" />
        <meta name="spiders" content="all" />
        <title>@yield('title')</title>
        <link rel="shortcut icon" href="" />

        <link type="text/css" rel="stylesheet" href="{{ asset('/admin/css/bootstrap.min.css') }}" />
        <link type="text/css" rel="stylesheet" href="{{ asset('/admin/css/font-awesome.min.css') }}" />
        <link type="text/css" rel="stylesheet" href="{{ asset('/admin/css/dashboard.css') }}" />
        <link type="text/css" rel="stylesheet" href="{{ asset('/admin/css/responsive.css') }}" />
        <link type="text/css" rel="stylesheet" href="{{ asset('/admin/css/dataTables.bootstrap.min.css') }}" />
        <link type="text/css" rel="stylesheet" href="{{ asset('/admin/css/responsive.bootstrap.min.css') }}" />
        <link type="text/css" rel="stylesheet" href="{{ asset('/admin/js/filemanager/fancybox/jquery.fancybox.css') }}" />
        <link type="text/css" rel="stylesheet" href="{{ asset('/admin/css/patternlock.css') }}" />
        <link type="text/css" rel="stylesheet" href="{{ asset('/admin/css/bootstrap-tagsinput.css') }}" />
        <link type="text/css" rel="stylesheet" href="{{ asset('/admin/css/bootstrap-datetimepicker.min.css') }}" />
        <link type="text/css" rel="stylesheet" href="{{ asset('/admin/css/bootstrap-editable.css') }}" />
        <link type="text/css" rel="stylesheet" href="{{ asset('/admin/css/dropzone.css') }}" />

        @stack('css')

        <script type="text/javascript" src="{{ asset('/admin/js/jquery/jquery-2.1.4.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/admin/js/bootstrap/bootstrap.min.js') }}"></script>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <script type="text/javascript">
            var BASE_URL = '{{ url('') }}'; 
        </script>

    </head>
    <body class="fixed-left">
        <div id="wrapper">
            <div class="topbar">
                <div class="topbar-left">
                    <div class="text-center">
                        <a href="#" class="logo"><span>Admin Panel</span></a>
                    </div>
                </div>
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">
                        <div class="row-menu">
                            <div class="pull-left hidden-xs hidden-sm">
                                <button class="button-menu-mobile open-left"><i class="fa fa-navicon"></i></button>
                                <span class="clearfix"></span>
                            </div>
                            <div class="pull-left visible-xs visible-sm">
                                <a href="#" class="button-menu-mobile"><i class="fa fa-home"></i></a>
                                <span class="clearfix"></span>
                            </div>
                            <ul class="nav navbar-nav navbar-right pull-right">
                                <li style="float:left;">
                                    <a href="{{ url('admin/' . request()->segment(2)) }}/create" style="color:#3498db !important;">
                                        <span class="fa-stack fa-lg">
                                            <i class="fa fa-circle fa-stack-2x"></i>
                                            <i class="fa fa-plus fa-stack-1x fa-inverse"></i>
                                        </span>
                                    </a>
                                </li>
                                <li class="dropdown" style="float:left;">
                                    <a href="#" class="dropdown-toggle profile" data-toggle="dropdown" aria-expanded="true">
                                        <img src="http://balitbangda.papuabaratprov.go.id/po-content/uploads/user-editor.jpg" alt="" width="30" height="30" class="img-circle" title="" />
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="" target="_blank"><i class="fa fa-desktop"></i>&nbsp;&nbsp; Ke Front End</a></li>
                                        <li class="divider visible-lg"></li>
                                        <li><a href="#"><i class="fa fa-user"></i>&nbsp;&nbsp; Edit Profil</a></li>
                                        <li class="divider visible-lg"></li>
                                        <li><a href="{{ route('admin.logout') }}"><i class="fa fa-ban"></i>&nbsp;&nbsp; Logout</a></li>
                                    </ul>
                                </li>
                                <li class="menu-btn"><a href="javascript:void(0)"><i class="fa fa-bars"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">
                    <div id="sidebar-menu">
                        
                        @include('admin.layout.menu')

                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="content-page">
                <nav class="dark-sidebar">
                    
                </nav>
                <div class="content">
                    <div class="container">
                        
                        @yield('content')

                    </div>
                </div>
            </div>
        </div>
        <div id="alert-multiple-delete" class="modal fade" tabindex="-1" role="dialog" aria-hidden="false">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 id="modal-title"><i class="fa fa-exclamation-triangle text-danger"></i> Delete Confirmation </h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="delid" name="id" value="3">
                        Are you sure you want to delete this data ?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-confirm btn-danger"><i class="fa fa-trash-o"></i> Yes</button>
                        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal" aria-hidden="true"><i class="fa fa-sign-out"></i> No</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="alert-delete" class="modal fade" tabindex="-1" role="dialog" aria-hidden="false">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action="" method="POST" id="form-delete">
                        @csrf 
                        @method('DELETE')
                        <input type="hidden" name="id">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 id="modal-title"><i class="fa fa-exclamation-triangle text-danger"></i> Delete Confirmation </h4>
                        </div>
                        <div class="modal-body">
                            <input type="hidden" id="delid" name="id" value="3">
                            Are you sure you want to delete this data ?
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-trash-o"></i> Yes</button>
                            <button type="button" class="btn btn-sm btn-default" data-dismiss="modal" aria-hidden="true"><i class="fa fa-sign-out"></i> No</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            var resizefunc = [];
        </script>
        <script type="text/javascript" src="{{ asset('/admin/js/tinymce/tinymce.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/admin/js/detect/detect.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/admin/js/fastclick/fast-click.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/admin/js/slimscroll/jquery.slimscroll.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/admin/js/datatables/jquery.dataTables.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/admin/js/datatables/dataTables.bootstrap.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/admin/js/datatables/extensions/Responsive/js/dataTables.responsive.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/admin/js/datatables/extensions/Responsive/js/responsive.bootstrap.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/admin/js/filemanager/fancybox/jquery.fancybox.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/admin/js/patternlock/patternLock.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/admin/js/tagsinput/bootstrap-tagsinput.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/admin/js/tagsinput/typeahead.bundle.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/admin/js/datetime/moment.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/admin/js/datetime/bootstrap-datetimepicker.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/admin/js/maskedinput/jquery.maskedinput.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/admin/js/xeditable/bootstrap-editable.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/admin/js/filestyle/bootstrap-filestyle.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/admin/js/chartjs/chart.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/admin/js/dropzone/dropzone.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/admin/js/dashboard-core.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/admin/js/app.js') }}"></script>

        @stack('js')

    </body>
</html>