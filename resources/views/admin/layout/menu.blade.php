<ul id="main_menu">
    <li>
        <a href="{{ route('admin.dashboard') }}"><i class="fa fa-home fa-fw"></i> <span>Dashboard</span></a>
    </li>
    <li class="has_sub">
        <a href="#"><i class="fa fa-book fa-fw"></i> <span>Posts</span></a>
        <ul class="list-unstyled">
            <li><a href="{{ route('admin.post') }}"><span>All Posts</span></a></li>
            <li><a href="{{ route('admin.post.create') }}"><span>Add New Post</span></a></li>
        </ul>
    </li>
    <li class="has_sub">
        <a href="#">
            <i class="fa fa-bookmark-o fa-fw"></i> 
            <span>Categories</span>
        </a>
        <ul class="list-unstyled">
            <li><a href="{{ route('admin.category') }}"><span>All Categories</span></a></li>
            <li><a href="{{ route('admin.category.create') }}"><span>Add New Category</span></a></li>
        </ul>
    </li>
    @php 
    $auth = Auth::guard('admin'); 
    $check = $auth->user()->roles()->whereHas('role', function($model) {
        $model->where('slug', 'admin'); 
    })->first(); 
    @endphp 
    @if ($check)
        <li class="has_sub">
            <a href="#"><i class="fa fa-users fa-fw"></i> <span>Users</span></a>
            <ul class="list-unstyled">
                <li><a href="{{ route('admin.user') }}"><span>All Users</span></a></li>
                <li><a href="{{ route('admin.role') }}"><span>Roles</span></a></li>
                <li><a href="{{ route('admin.permission') }}"><span>Permissions</span></a></li>
            </ul>
        </li>
    @endif
</ul>