@extends('admin.layout.app')

@push('css')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@section('content')
    <div class="block-content">
        <form action="{{ route($form['_action']) }}" method="{{ __($form['_method']) }}">
            @csrf 
            @if (isset($data))
                @method('PUT')
                <input type="hidden" name="id" value="{{ $data->id }}">
            @endif
            <div class="row">
                <div class="col-md-12">
                    <div class="block-header">
                        <h3>{{ __($title) }}</h3>
                        <ol class="list-inline list-unstyled">
                            <li><a href="{{ route('admin.dashboard') }}">Beranda</a></li>
                            <li>/</li>
                            <li class="active">{{ __($title) }}</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="slug">Slug <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="slug" name="slug" value="{{ isset($data) ? $data->slug : '' }}" placeholder="" required="">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="title">Title <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="title" name="title" value="{{ isset($data) ? $data->title : '' }}" placeholder="" required="">
                    </div>
                </div>
                <div class="col-md-12">
                <div class="form-group">
                        <label for="permissions">Permissions <span class="text-danger">*</span></label>
                        <select name="permissions[]" id="permissions" class="form-control" multiple="multiple">
                            @foreach ($permissions as $permission)
                                <option value="{{ $permission->id }}" {{ is_null($permission->selected) ? '' : 'selected' }}>{{ $permission->title }}</option>
                            @endforeach 
                        </select>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Submit</button>
                        <button type="reset" class="btn btn-danger pull-right" onclick="self.history.back()"><i class="fa fa-times"></i> Cancel</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@push('js')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
    <script>
        $('#permissions').select2(); 
    </script>
@endpush    