@extends('admin.layout.app')

@push('css')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@section('content')
    <div class="block-content">
        <form action="{{ route($form['_action']) }}" method="{{ __($form['_method']) }}">
            @csrf 
            @if (isset($data))
                @method('PUT')
                <input type="hidden" name="id" value="{{ $data->id }}">
            @endif
            <div class="row">
                <div class="col-md-12">
                    <div class="block-header">
                        <h3>{{ __($title) }}</h3>
                        <ol class="list-inline list-unstyled">
                            <li><a href="{{ route('admin.dashboard') }}">Beranda</a></li>
                            <li>/</li>
                            <li class="active">{{ __($title) }}</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name">Name <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="{{ isset($data) ? $data->name : '' }}" placeholder="" required="">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name">Email <span class="text-danger">*</span></label>
                        <input type="email" class="form-control" id="email" name="email" value="{{ isset($data) ? $data->email : '' }}" placeholder="" required="">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="roles">Roles <span class="text-danger">*</span></label>
                        <select name="roles[]" id="roles" class="form-control" multiple="multiple">
                            @foreach ($roles as $role)
                                <option value="{{ $role->id }}" {{ is_null($role->selected) ? '' : 'selected' }}>{{ $role->title }}</option>
                            @endforeach 
                        </select>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="permissions">Permissions</label>
                        <select name="permissions[]" id="permissions" class="form-control" multiple="multiple">
                            @foreach ($permissions as $permission)
                                <option value="{{ $permission->id }}" {{ is_null($permission->selected) ? '' : 'selected' }}>{{ $permission->title }}</option>
                            @endforeach 
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" id="password" name="password" value="" placeholder="">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="password_confirm">Password Confirmation</label>
                        <input type="password" class="form-control" id="password_confirm" name="password_confirm" value="" placeholder="">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Submit</button>
                        <button type="reset" class="btn btn-danger pull-right" onclick="self.history.back()"><i class="fa fa-times"></i> Cancel</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@push('js')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
    <script>
        $('#roles').select2(); 
        $('#permissions').select2(); 
    </script>
@endpush    