<?php

use Illuminate\Database\Seeder;

use App\Models\AdminRoles; 

class AdminRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AdminRoles::create([
            'slug'  => 'admin', 
            'title' => 'Admin'
        ]); 

        AdminRoles::create([
            'slug'  => 'author', 
            'title' => 'Author'
        ]); 
    }
}
