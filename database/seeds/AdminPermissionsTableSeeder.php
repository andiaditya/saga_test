<?php

use Illuminate\Database\Seeder;

use App\Models\AdminPermissions; 

class AdminPermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AdminPermissions::create([
            'id'            => 1, 
            'slug'          => 'all.permission', 
            'title'         => 'All Permission', 
            'http_method'   => 'GET,POST,PUT,DELETE', 
            'http_path'     => '/*'
        ]); 

        AdminPermissions::create([
            'id'            => 2, 
            'slug'          => 'category', 
            'title'         => 'Category', 
            'http_method'   => 'GET,POST,PUT,DELETE', 
            'http_path'     => '/category*'
        ]); 

        AdminPermissions::create([
            'id'            => 3, 
            'slug'          => 'category', 
            'title'         => 'Category', 
            'http_method'   => 'GET,POST,PUT,DELETE', 
            'http_path'     => '/category*'
        ]); 

        AdminPermissions::create([
            'id'            => 4, 
            'slug'          => 'post', 
            'title'         => 'Post', 
            'http_method'   => 'GET,POST,PUT,DELETE', 
            'http_path'     => '/posts*'
        ]); 

        AdminPermissions::create([
            'id'            => 5, 
            'slug'          => 'dashboard', 
            'title'         => 'Dashboad', 
            'http_method'   => 'GET,POST,PUT,DELETE', 
            'http_path'     => '/dashboard'
        ]); 
    }
}
