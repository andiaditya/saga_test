<?php

use Illuminate\Database\Seeder;

use App\Models\PostsCategories; 

class PostsCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PostsCategories::create([
            'post_id'       => 1,
            'category_id'   => 1
        ]); 

        PostsCategories::create([
            'post_id'       => 2,
            'category_id'   => 2
        ]); 

        PostsCategories::create([
            'post_id'       => 3,
            'category_id'   => 3
        ]); 

        PostsCategories::create([
            'post_id'       => 4,
            'category_id'   => 4
        ]); 

        PostsCategories::create([
            'post_id'       => 5,
            'category_id'   => 1
        ]); 

        PostsCategories::create([
            'post_id'       => 6,
            'category_id'   => 3
        ]); 
    }
}
