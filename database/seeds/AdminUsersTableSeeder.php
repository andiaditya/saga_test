<?php

use Illuminate\Database\Seeder;

use App\Models\AdminUsers; 

class AdminUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create Admin
        AdminUsers::create([
            'id'            => 1,
            'name'          => 'Admin', 
            'email'         => 'admin@gmail.com', 
            'avatar'        => '', 
            'provider'      => 'web',
            'provider_id'   => '', 
            'password'      => '$12$ikl/zvevueQKXvF3Abw80u/yMLELu6SO8fzAO8iU8oyg6fgdWIoFe'
        ]);
        
        // Create Author
        AdminUsers::create([
            'id'            => 2, 
            'name'          => 'Andi Aditya', 
            'email'         => 'freelancer.andi@gmail.com', 
            'avatar'        => '', 
            'provider'      => 'web',
            'provider_id'   => '', 
            'password'      => '$10$DVOnYyhOJRZWfUXtdUXUSOO.0PcHaH7v4dbfQYKjegwTxCY73vLVK'
        ]); 
    }
}
