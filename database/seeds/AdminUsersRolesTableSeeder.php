<?php

use Illuminate\Database\Seeder;

use App\Models\AdminUsersRoles; 

class AdminUsersRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AdminUsersRoles::create([
            'user_id' => 1,
            'role_id' => 1
        ]); 

        AdminUsersRoles::create([
            'user_id' => 2,
            'role_id' => 2
        ]); 
    }
}
