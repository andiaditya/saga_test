<?php

use Illuminate\Database\Seeder;

use App\Models\Categories; 

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Categories::create([
            'id'        => 1, 
            'slug'      => 'my-indonesia',
            'title'     => 'My Indonesia'
        ]); 

        Categories::create([
            'id'        => 2,
            'slug'      => 'culinary-tour',
            'title'     => 'Culinary Tour'
        ]); 

        Categories::create([
            'id'        => 3, 
            'slug'      => 'success',
            'title'     => 'Success'
        ]); 

        Categories::create([
            'id'        => 4, 
            'slug'      => 'relationship',
            'title'     => 'Relationship'
        ]); 

    }
}
