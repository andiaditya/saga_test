<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AdminUsersTableSeeder::class);
        $this->call(AdminPermissionsTableSeeder::class);
        $this->call(AdminRolesTableSeeder::class);
        $this->call(AdminUsersRolesTableSeeder::class); 
        $this->call(CategoriesTableSeeder::class); 
        $this->call(PostsTableSeeder::class); 
        $this->call(PostsCategoriesTableSeeder::class); 
    }
}
