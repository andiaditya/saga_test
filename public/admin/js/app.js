$('.form-multiple-delete').on('submit', function(e) {
    e.preventDefault(); 
    $('#alert-multiple-delete').modal('show'); 
})

$('.btn-confirm').on('click', function() {
    $('.form-multiple-delete').unbind().submit(); 
}); 

$("#checkAll").click(function () {
    $('.check-id').not(this).prop('checked', this.checked).change();
});

function show_delete_button(e) {
    var id = e.getAttribute('data-id'); 
    var route = e.getAttribute('data-action'); 
    // Set Form Delete 
    $('#alert-delete input[name="id"]').val(id);
    $('#alert-delete #form-delete').attr('action', route);
    $('#alert-delete').modal('show');  
}