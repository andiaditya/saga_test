<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Front\HomeController@index')->name('home');
Route::get('category/{slug}', 'Front\CategoriesController@index')->name('category');
Route::get('news/{slug}', 'Front\PostsController@detail')->name('post.detail'); 

// Show Form Login
Route::get('admin/login', 'Admin\LoginController@index')->name('admin.login.form');
Route::get('login', 'Admin\LoginController@index')->name('admin.login.form');  
// Login Process
Route::post('login', 'Admin\LoginController@loginProcess')->name('admin.login.process');
// By Social Media 
Route::get('socmed/{provider}', 'Admin\SocmedController@redirectToProvider');
Route::get('socmed/{provider}/callback', 'Admin\SocmedController@handleProviderCallback');

Route::group(['middleware' => ['author']], function () {
    Route::prefix('admin')->group(function () {
        // Logout 
        Route::get('logout', 'Admin\LoginController@logout')->name('admin.logout'); 
        // Dashboard 
        Route::get('dashboard', 'Admin\DashboardController@index')->name('admin.dashboard'); 
        // Category
        Route::prefix('post')->group(function () {
            Route::get('/', 'Admin\PostsController@index')->name('admin.post'); 
            Route::get('create', 'Admin\PostsController@create')->name('admin.post.create'); 
            Route::post('store', 'Admin\PostsController@store')->name('admin.post.store');
            Route::get('json-tables', 'Admin\PostsController@jsonTables')->name('admin.post.json-tables'); 
            Route::get('edit/{id}', 'Admin\PostsController@edit')->name('admin.post.edit'); 
            Route::put('update', 'Admin\PostsController@update')->name('admin.post.update'); 
            Route::delete('delete', 'Admin\PostsController@delete')->name('admin.post.delete');
            Route::delete('delete-multiple', 'Admin\PostsController@deleteMultiple')->name('admin.post.delete-multiple');  
        }); 
        // Category
        Route::prefix('category')->group(function () {
            Route::get('/', 'Admin\AdminCategoriesController@index')->name('admin.category'); 
            Route::get('create', 'Admin\AdminCategoriesController@create')->name('admin.category.create'); 
            Route::post('store', 'Admin\AdminCategoriesController@store')->name('admin.category.store');
            Route::get('json-tables', 'Admin\AdminCategoriesController@jsonTables')->name('admin.category.json-tables'); 
            Route::get('json-select', 'Admin\AdminCategoriesController@jsonSelect')->name('admin.category.json-select'); 
            Route::get('edit/{id}', 'Admin\AdminCategoriesController@edit')->name('admin.category.edit'); 
            Route::put('update', 'Admin\AdminCategoriesController@update')->name('admin.category.update'); 
            Route::delete('delete', 'Admin\AdminCategoriesController@delete')->name('admin.category.delete');
            Route::delete('delete-multiple', 'Admin\AdminCategoriesController@deleteMultiple')->name('admin.category.delete-multiple');  
        }); 

        Route::group(['middleware' => ['admin']], function () {
            // User 
            Route::prefix('user')->group(function () {
                Route::get('/', 'Admin\AdminUsersController@index')->name('admin.user'); 
                Route::get('create', 'Admin\AdminUsersController@create')->name('admin.user.create'); 
                Route::post('store', 'Admin\AdminUsersController@store')->name('admin.user.store');
                Route::get('json-tables', 'Admin\AdminUsersController@jsonTables')->name('admin.user.json-tables'); 
                Route::get('edit/{id}', 'Admin\AdminUsersController@edit')->name('admin.user.edit'); 
                Route::put('update', 'Admin\AdminUsersController@update')->name('admin.user.update'); 
                Route::delete('delete', 'Admin\AdminUsersController@delete')->name('admin.user.delete');
                Route::delete('delete-multiple', 'Admin\AdminUsersController@deleteMultiple')->name('admin.user.delete-multiple');  
            }); 
            // Role
            Route::prefix('role')->group(function () {
                Route::get('/', 'Admin\AdminRolesController@index')->name('admin.role'); 
                Route::get('create', 'Admin\AdminRolesController@create')->name('admin.role.create'); 
                Route::post('store', 'Admin\AdminRolesController@store')->name('admin.role.store');
                Route::get('json-tables', 'Admin\AdminRolesController@jsonTables')->name('admin.role.json-tables'); 
                Route::get('edit/{id}', 'Admin\AdminRolesController@edit')->name('admin.role.edit'); 
                Route::put('update', 'Admin\AdminRolesController@update')->name('admin.role.update'); 
                Route::delete('delete', 'Admin\AdminRolesController@delete')->name('admin.role.delete');
                Route::delete('delete-multiple', 'Admin\AdminRolesController@deleteMultiple')->name('admin.role.delete-multiple');    
            }); 
            // Permission
            Route::prefix('permission')->group(function () {
                Route::get('/', 'Admin\AdminPermissionsController@index')->name('admin.permission'); 
                Route::get('create', 'Admin\AdminPermissionsController@create')->name('admin.permission.create'); 
                Route::post('store', 'Admin\AdminPermissionsController@store')->name('admin.permission.store');
                Route::get('json-tables', 'Admin\AdminPermissionsController@jsonTables')->name('admin.permission.json-tables'); 
                Route::get('edit/{id}', 'Admin\AdminPermissionsController@edit')->name('admin.permission.edit'); 
                Route::put('update', 'Admin\AdminPermissionsController@update')->name('admin.permission.update'); 
                Route::delete('delete', 'Admin\AdminPermissionsController@delete')->name('admin.permission.delete');
                Route::delete('delete-multiple', 'Admin\AdminPermissionsController@deleteMultiple')->name('admin.permission.delete-multiple');  
            }); 
        }); 
    }); 
}); 