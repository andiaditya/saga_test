<?php

namespace App\Http\Middleware;

use Auth; 
use Closure;

class Admin 
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    public function handle($request, Closure $next)
    {
        $auth = Auth::guard('admin'); 
        if ( ! $auth->check()) {
            return redirect()->route('admin.login.form'); 
        }

        if (is_null($this->checkAdmin())) {
            return abort(403);
        }

        return $next($request);
    }

    public function checkAdmin() {
        $auth = Auth::guard('admin'); 
        $check = $auth->user()->roles()->whereHas('role', function($model) {
            $model->where('slug', 'admin'); 
        })->first(); 

        return $check; 
    }
}
