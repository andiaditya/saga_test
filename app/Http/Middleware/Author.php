<?php

namespace App\Http\Middleware;

use Auth; 
use Closure;

class Author 
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    public function handle($request, Closure $next)
    {
        $auth = Auth::guard('admin'); 
        if ( ! $auth->check()) {
            return redirect()->route('admin.login.form'); 
        }

        return $next($request);
    }
}
