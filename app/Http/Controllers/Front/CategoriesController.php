<?php 

namespace App\Http\Controllers\Front;

use App\Models\Posts; 
use App\Models\Categories; 
use App\Http\Controllers\Front\FrontController; 

class CategoriesController extends FrontController {

    /**
     * Show Index Website 
     * 
     * @return void
     */
    public function index($slug) {
        // Check Categories
        $category = Categories::where("slug", $slug)->first(); 
        // If Not Found Abort 404
        if (empty($category)) {
            abort(404); 
        }
        // Get Related Post By Category
        $posts = Posts::whereHas('categories', function($model) use ($category) {
            $model->where('category_id', $category->id); 
        })->orderBy('id', 'DESC')->paginate(5); 

        return view('front.categories', [
            'posts' => $posts, 
            'category' => $category, 
            'page' => [
                'title'     => '', 
                'desc'      => '', 
                'keyword'   => ''
            ]
        ]); 
    }
}