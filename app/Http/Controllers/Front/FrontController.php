<?php 

namespace App\Http\Controllers\Front; 

use App\Models\Categories; 
use App\Http\Controllers\Controller;

use View; 

class FrontController extends Controller {

    public function __construct() {
        View::share('categories', Categories::all()); 
    }
}