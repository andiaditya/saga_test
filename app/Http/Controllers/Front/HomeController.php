<?php 

namespace App\Http\Controllers\Front;

use App\Models\Posts; 
use App\Http\Controllers\Front\FrontController; 

class HomeController extends FrontController {

    /**
     * Show Index Website 
     * 
     * @return void
     */
    public function index() {
        return view('front.home', [
            'posts' => Posts::orderBy('id', 'DESC')
                ->limit(5)
                ->get(), 
            'page' => [
                'title'     => '', 
                'desc'      => '', 
                'keyword'   => ''
            ]
        ]); 
    }
}