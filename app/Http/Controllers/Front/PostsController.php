<?php 

namespace App\Http\Controllers\Front;

use App\Models\Posts; 
use App\Http\Controllers\Front\FrontController; 

class PostsController extends FrontController {

    /**
     * Show Index Website 
     * 
     * @return void
     */
    public function detail($slug) {
        // Check Posts
        $post = Posts::where("slug", $slug)->first(); 
        // If Not Found Abort 404
        if (empty($post)) {
            abort(404); 
        }

        return view('front.detailpost', [ 
            'post' => $post, 
            'page' => [
                'title'     => '', 
                'desc'      => '', 
                'keyword'   => ''
            ]
        ]); 
    }
}