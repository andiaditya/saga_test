<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Guard Authentification
     * 
     * @var string 
     */
    protected $guard = 'admin'; 

    /**
     * Guard 
     */
    public function guard()
    {
        return auth()->guard($this->guard);
    }
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    
    /**
     * Show Login Form 
     * 
     * @return void
     */
    public function index() {
        return view('admin.auth.login'); 
    }

    /**
     * Login Proses 
     * 
     * @return void
     */
    public function loginProcess() {
        if (auth()->guard('admin') ->attempt(['email' => request()->email, 'password' => request()->password])) {
            return redirect()->route('admin.dashboard'); 
        }

        return back()->withErrors(['email' => 'Email atau password salah']); 
    }

    /**
     * Logout 
     * 
     * @return void
     */
    public function logout() {
        \Auth::guard('admin')->logout(); 

        return redirect()->route('admin.login.form'); 
    }
}
