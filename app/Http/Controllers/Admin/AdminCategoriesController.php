<?php 

namespace App\Http\Controllers\Admin; 

use App\Http\Controllers\Controller;
use App\Repositories\Admin\CategoriesRepository; 

class AdminCategoriesController extends Controller {

    /**
     * Modules Title 
     * 
     * @var string
     */
    protected $title = 'Category';    

    /**
     * Repository Object 
     */
    protected $categoriesRepository; 

    /**
     * Construct Class
     * 
     * @return void
     */
    public function __construct() {
        $this->categoriesRepository = new CategoriesRepository; 
    }

    /**
     * Index 
     * 
     * @return void
     */
    public function index() {
        return view('admin.categories.index')
            ->with('title', $this->title); 
    }

    /**
     * Get Json Data 
     * 
     * @return json
     */
    public function jsonTables() {
        return $this->categoriesRepository->jsonTables(); 
    }

    /**
     * Get Json Select 
     * 
     * @return json
     */
    public function jsonSelect() {
        return $this->categoriesRepository->jsonSelect(); 
    }

    /**
     * Create 
     * 
     * @return void
     */
    public function create() {
        return view('admin.categories.form', [
            'title' => 'Add ' . $this->title, 
            'form' => [
                '_action' => 'admin.category.store', 
                '_method' => 'POST'
            ]
        ]); 
    }

    /**
     * Edit 
     * 
     * @return void
     */
    public function edit($id) {
        $data = $this->categoriesRepository->categories->findOrFail($id); 
        return view('admin.categories.form', [
            'title' => 'Edit ' . $this->title, 
            'data' => $data, 
            'form' => [
                '_action' => 'admin.category.update', 
                '_method' => 'POST'
            ]
        ]); 
    }

    /**
     * Store Data 
     * 
     * @return void
     */
    public function store() {
        $this->categoriesRepository->create(); 
        // Redirect With Success Message
        return redirect()->route('admin.category')
            ->with('message', 'Data Category Successfully Saved'); 
    }

    /**
     * Update Data 
     * 
     * @return void
     */
    public function update() {
        $this->categoriesRepository->update(); 
        // Redirect With Success Message
        return redirect()->route('admin.category')
            ->with('message', 'Data Category Successfully Saved'); 
    }

    /**
     * Delete 
     * 
     * @return void
     */
    public function delete() {
        $deleted = $this->categoriesRepository->delete(); 
        // Redirect With Success Message
        if ($deleted) {
            return redirect()->route('admin.category')
                ->with('message', 'Data Category Successfully Deleted');
        } else {
            return redirect()->route('admin.category')
                ->with('message', 'Data Category Not Found');
        }
    }

    /**
     * Delete Multiple
     * 
     * @return void
     */
    public function deleteMultiple() {
        $deleted = $this->categoriesRepository->deleteMultiple(); 
        // Redirect With Success Message
        if ($deleted) {
            return redirect()->route('admin.category')
                ->with('message', 'Data Category Successfully Deleted');
        } else {
            return redirect()->route('admin.category')
                ->with('message', 'No data selected');
        }
    }
}