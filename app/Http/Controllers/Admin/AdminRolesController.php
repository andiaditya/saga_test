<?php 

namespace App\Http\Controllers\Admin; 

use App\Http\Controllers\Controller;
use App\Repositories\Admin\AdminRolesRepository; 
use App\Repositories\Admin\AdminPermissionsRepository; 
use App\Repositories\Admin\AdminRolesPermissionsRepository; 
use App\Validation\Admin\AdminRolesValidation; 

class AdminRolesController extends Controller {

    /**
     * Modules Title 
     * 
     * @var string
     */
    protected $title = 'Role';    

    /**
     * Repository Object 
     */
    protected $adminRolesRepository; 
    protected $adminPermissionsRepository; 
    protected $adminRolesPermissionsRepository; 

    /**
     * Construct Class
     * 
     * @return void
     */
    public function __construct() {
        $this->adminRolesRepository = new AdminRolesRepository; 
        $this->adminPermissionsRepository = new AdminPermissionsRepository; 
        $this->adminRolesPermissionsRepository = new AdminRolesPermissionsRepository; 
    }

    /**
     * Index 
     * 
     * @return void
     */
    public function index() {
        return view('admin.roles.index')
            ->with('title', $this->title); 
    }

    /**
     * Get Json Data 
     * 
     * @return json
     */
    public function jsonTables() {
        return $this->adminRolesRepository->jsonTables(); 
    }

    /**
     * Create 
     * 
     * @return void
     */
    public function create() {
        return view('admin.roles.form', [
            'title' => 'Add ' . $this->title, 
            'permissions' => $this->adminPermissionsRepository->adminPermissions->all(), 
            'form' => [
                '_action' => 'admin.role.store', 
                '_method' => 'POST'
            ]
        ]); 
    }

    /**
     * Edit 
     * 
     * @return void
     */
    public function edit($id) {
        $data = $this->adminRolesRepository->adminRoles->findOrFail($id); 
        return view('admin.roles.form', [
            'title' => 'Edit ' . $this->title, 
            'data' => $data, 
            'permissions' => $this->adminRolesRepository->getDefaultPermission($id), 
            'form' => [
                '_action' => 'admin.role.update', 
                '_method' => 'POST'
            ]
        ]); 
    }

    /**
     * Store Data 
     * 
     * @return void
     */
    public function store() {
        // Validate 
        $validation = new AdminRolesValidation; 
        $validation->run(); 
        // Store Data 
        $role = $this->adminRolesRepository->create(); 
        // Create Permission
        foreach (request()->permissions as $permission) {
            $this->adminRolesPermissionsRepository->adminRolesPermissions->create([
                'role_id' => $role->id,
                'permission_id' => $permission
            ]); 
        }
        // Redirect With Success Message
        return redirect()->route('admin.role')
            ->with('message', 'Data Role Successfully Saved'); 
    }

    /**
     * Update Data 
     * 
     * @return void
     */
    public function update() {
        // Validate 
        $validation = new AdminRolesValidation; 
        $validation->run(); 
        // Store Data 
        $role = $this->adminRolesRepository->update(); 
        // Delete Data Item 
        $this->adminRolesPermissionsRepository->deleteByRole(); 
        // Create Permission
        foreach (request()->permissions as $permission) {
            $this->adminRolesPermissionsRepository->adminRolesPermissions->create([
                'role_id' => $role->id,
                'permission_id' => $permission
            ]); 
        }
        return redirect()->route('admin.role')
            ->with('msg', 'Data Role Successfully Saved'); 
    }

    /**
     * Delete 
     * 
     * @return void
     */
    public function delete() {
        $deleted = $this->adminRolesRepository->delete(); 
        // Redirect With Success Message
        if ($deleted) {
            return redirect()->route('admin.role')
                ->with('message', 'Data Role Successfully Deleted');
        } else {
            return redirect()->route('admin.role')
                ->with('message', 'Data Role Not Found');
        }
    }

    /**
     * Delete Multiple
     * 
     * @return void
     */
    public function deleteMultiple() {
        $deleted = $this->adminRolesRepository->deleteMultiple(); 
        // Redirect With Success Message
        if ($deleted) {
            return redirect()->route('admin.role')
                ->with('message', 'Data Role Successfully Deleted');
        } else {
            return redirect()->route('admin.role')
                ->with('message', 'No data selected');
        }
    }
}