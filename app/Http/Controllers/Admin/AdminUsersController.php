<?php 

namespace App\Http\Controllers\Admin; 

use App\Http\Controllers\Controller;
use App\Repositories\Admin\AdminRolesRepository; 
use App\Repositories\Admin\AdminUsersRepository;
use App\Repositories\Admin\AdminUsersRolesRepository;  
use App\Repositories\Admin\AdminPermissionsRepository; 
use App\Repositories\Admin\AdminUsersPermissionsRepository;
use App\Validation\Admin\AdminUsersValidation; 

class AdminUsersController extends Controller {

    /**
     * Modules Title 
     * 
     * @var string
     */
    protected $title = 'Users';    

    /**
     * Repository Object 
     */
    protected $adminRolesRepository; 
    protected $adminUsersRepository; 
    protected $adminPermissionsRepository; 
    protected $adminUsersPermissionsRepository; 
    protected $adminUsersRolesRepository; 

    /**
     * Construct Class
     * 
     * @return void
     */
    public function __construct() {
        $this->adminRolesRepository = new AdminRolesRepository; 
        $this->adminUsersRepository = new AdminUsersRepository; 
        $this->adminPermissionsRepository = new AdminPermissionsRepository; 
        $this->adminUsersPermissionsRepository = new AdminUsersPermissionsRepository; 
        $this->adminUsersRolesRepository = new AdminUsersRolesRepository; 
    }

    /**
     * Index 
     * 
     * @return void
     */
    public function index() {
        return view('admin.users.index')
            ->with('title', $this->title); 
    }

    /**
     * Get Json Data 
     * 
     * @return json
     */
    public function jsonTables() {
        return $this->adminUsersRepository->jsonTables(); 
    }

    /**
     * Create 
     * 
     * @return void
     */
    public function create() {
        return view('admin.users.form', [
            'title' => 'Add ' . $this->title, 
            'roles' => $this->adminRolesRepository->adminRoles->all(), 
            'permissions' => $this->adminPermissionsRepository->adminPermissions->all(), 
            'form' => [
                '_action' => 'admin.user.store', 
                '_method' => 'POST'
            ]
        ]); 
    }

    /**
     * Edit 
     * 
     * @return void
     */
    public function edit($id) {
        $data = $this->adminUsersRepository->adminUsers->findOrFail($id); 
        return view('admin.users.form', [
            'title' => 'Edit ' . $this->title, 
            'data' => $data, 
            'roles' => $this->adminUsersRepository->getDefaultRole($id), 
            'permissions' => $this->adminUsersRepository->getDefaultPermission($id), 
            'form' => [
                '_action' => 'admin.user.update', 
                '_method' => 'POST'
            ]
        ]); 
    }

    /**
     * Store Data 
     * 
     * @return void
     */
    public function store() {
        // Store Data 
        $user = $this->adminUsersRepository->create(); 
        // Create Roles 
        if (is_array(request()->roles)) {
            foreach (request()->roles as $role) {
                $this->adminUsersRolesRepository->adminUsersRoles->create([
                    'role_id' => $role,
                    'user_id' => $user->id
                ]); 
            }
        }
        // Create Permission
        if (is_array(request()->permissions)) {
            foreach (request()->permissions as $permission) {
                $this->adminUsersPermissionsRepository->adminUsersPermissions->create([
                    'user_id' => $user->id,
                    'permission_id' => $permission
                ]); 
            }
        }
        // Redirect With Success Message
        return redirect()->route('admin.user')
            ->with('message', 'Data Role Successfully Saved'); 
    }

    /**
     * Update Data 
     * 
     * @return void
     */
    public function update() {
        // Store Data 
        $user = $this->adminUsersRepository->update(); 
        // Delete Data Item 
        $this->adminUsersPermissionsRepository->deleteByUser(); 
        // Create Permission
        foreach (request()->permissions as $permission) {
            $this->adminUsersPermissionsRepository->adminUsersPermissions->create([
                'user_id' => $user->id,
                'permission_id' => $permission
            ]); 
        }
        // Delete Data Item 
        $this->adminUsersRolesRepository->deleteByUser(); 
        // Create Permission
        foreach (request()->roles as $role) {
            $this->adminUsersRolesRepository->adminUsersRoles->create([
                'user_id' => $user->id,
                'role_id' => $role
            ]); 
        }
        return redirect()->route('admin.user')
            ->with('msg', 'Data Role Successfully Saved'); 
    }

    /**
     * Delete 
     * 
     * @return void
     */
    public function delete() {
        $deleted = $this->adminUsersRepository->delete(); 
        // Redirect With Success Message
        if ($deleted) {
            return redirect()->route('admin.user')
                ->with('message', 'Data Role Successfully Deleted');
        } else {
            return redirect()->route('admin.user')
                ->with('message', 'Data Role Not Found');
        }
    }

    /**
     * Delete Multiple
     * 
     * @return void
     */
    public function deleteMultiple() {
        $deleted = $this->adminUsersRepository->deleteMultiple(); 
        // Redirect With Success Message
        if ($deleted) {
            return redirect()->route('admin.user')
                ->with('message', 'Data Role Successfully Deleted');
        } else {
            return redirect()->route('admin.user')
                ->with('message', 'No data selected');
        }
    }
}