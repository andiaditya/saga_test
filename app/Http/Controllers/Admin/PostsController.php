<?php 

namespace App\Http\Controllers\Admin; 

use App\Http\Controllers\Controller;
use App\Repositories\Admin\PostsRepository; 
use App\Repositories\Admin\CategoriesRepository; 

class PostsController extends Controller {

    /**
     * Modules Title 
     * 
     * @var string
     */
    protected $title = 'Post';    

    /**
     * Repository Post
     */
    protected $postsRepository; 

    /**
     * Repository Category 
     */
    protected $categoriesRepository; 

    /**
     * Construct Class
     * 
     * @return void
     */
    public function __construct() {
        $this->postsRepository = new PostsRepository; 
        $this->categoriesRepository = new CategoriesRepository; 
    }

    /**
     * Index 
     * 
     * @return void
     */
    public function index() {
        return view('admin.posts.index')
            ->with('title', $this->title); 
    }

    /**
     * Get Json Data 
     * 
     * @return json
     */
    public function jsonTables() {
        return $this->postsRepository->jsonTables(); 
    }

    /**
     * Create 
     * 
     * @return void
     */
    public function create() {
        return view('admin.posts.form', [
            'title' => 'Add ' . $this->title, 
            'category' => $this->categoriesRepository->categories->all(), 
            'form' => [
                '_action' => 'admin.post.store', 
                '_method' => 'POST'
            ]
        ]); 
    }

    /**
     * Edit 
     * 
     * @return void
     */
    public function edit($id) {
        $data = $this->postsRepository->posts->findOrFail($id); 
        return view('admin.posts.form', [
            'title' => 'Edit ' . $this->title, 
            'category' => $this->postsRepository->getDefaultCategories($id), 
            'data' => $data, 
            'form' => [
                '_action' => 'admin.post.update', 
                '_method' => 'POST'
            ]
        ]); 
    }

    /**
     * Store Data 
     * 
     * @return void
     */
    public function store() {
        $this->postsRepository->create(); 
        // Redirect With Success Message
        return redirect()->route('admin.post')
            ->with('message', 'Data Post Successfully Saved'); 
    }

    /**
     * Update Data 
     * 
     * @return void
     */
    public function update() {
        $this->postsRepository->update(); 
        // Redirect With Success Message
        return redirect()->route('admin.post')
            ->with('message', 'Data Post Successfully Saved'); 
    }

    /**
     * Delete 
     * 
     * @return void
     */
    public function delete() {
        $deleted = $this->postsRepository->delete(); 
        // Redirect With Success Message
        if ($deleted) {
            return redirect()->route('admin.post')
                ->with('message', 'Data Post Successfully Deleted');
        } else {
            return redirect()->route('admin.post')
                ->with('message', 'Data Post Not Found');
        }
    }

    /**
     * Delete Multiple
     * 
     * @return void
     */
    public function deleteMultiple() {
        $deleted = $this->postsRepository->deleteMultiple(); 
        // Redirect With Success Message
        if ($deleted) {
            return redirect()->route('admin.post')
                ->with('message', 'Data Post Successfully Deleted');
        } else {
            return redirect()->route('admin.post')
                ->with('message', 'No data selected');
        }
    }
}