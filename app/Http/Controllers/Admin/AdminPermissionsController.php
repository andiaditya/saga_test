<?php 

namespace App\Http\Controllers\Admin; 

use App\Http\Controllers\Controller;
use App\Repositories\Admin\AdminPermissionsRepository; 

class AdminPermissionsController extends Controller {

    /**
     * Modules Title 
     * 
     * @var string
     */
    protected $title = 'Permission';    

    /**
     * Repository Object 
     */
    protected $adminPermissionsRepository; 

    /**
     * Construct Class
     * 
     * @return void
     */
    public function __construct() {
        $this->adminPermissionsRepository = new AdminPermissionsRepository; 
    }

    /**
     * Index 
     * 
     * @return void
     */
    public function index() {
        return view('admin.permissions.index')
            ->with('title', $this->title); 
    }

    /**
     * Get Json Data 
     * 
     * @return json
     */
    public function jsonTables() {
        return $this->adminPermissionsRepository->jsonTables(); 
    }

    /**
     * Create 
     * 
     * @return void
     */
    public function create() {
        return view('admin.permissions.form', [
            'title' => 'Add ' . $this->title, 
            'form' => [
                '_action' => 'admin.permission.store', 
                '_method' => 'POST'
            ]
        ]); 
    }

    /**
     * Edit 
     * 
     * @return void
     */
    public function edit($id) {
        $data = $this->adminPermissionsRepository->adminPermissions->findOrFail($id); 
        return view('admin.permissions.form', [
            'title' => 'Edit ' . $this->title, 
            'data' => $data, 
            'form' => [
                '_action' => 'admin.permission.update', 
                '_method' => 'POST'
            ]
        ]); 
    }

    /**
     * Store Data 
     * 
     * @return void
     */
    public function store() {
        $this->adminPermissionsRepository->create(); 
        // Redirect With Success Message
        return redirect()->route('admin.permission')
            ->with('message', 'Data Permission Successfully Saved'); 
    }

    /**
     * Update Data 
     * 
     * @return void
     */
    public function update() {
        $this->adminPermissionsRepository->update(); 
        // Redirect With Success Message
        return redirect()->route('admin.permission')
            ->with('msg', 'Data Permission Successfully Saved'); 
    }

    /**
     * Delete 
     * 
     * @return void
     */
    public function delete() {
        $deleted = $this->adminPermissionsRepository->delete(); 
        // Redirect With Success Message
        if ($deleted) {
            return redirect()->route('admin.permission')
                ->with('message', 'Data Permission Successfully Deleted');
        } else {
            return redirect()->route('admin.permission')
                ->with('message', 'Data Permission Not Found');
        }
    }

    /**
     * Delete Multiple
     * 
     * @return void
     */
    public function deleteMultiple() {
        $deleted = $this->adminPermissionsRepository->deleteMultiple(); 
        // Redirect With Success Message
        if ($deleted) {
            return redirect()->route('admin.permission')
                ->with('message', 'Data Permission Successfully Deleted');
        } else {
            return redirect()->route('admin.permission')
                ->with('message', 'No data selected');
        }
    }
}