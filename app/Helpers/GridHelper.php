<?php 

namespace App\Helpers; 

class GridHelper {

    /**
     * Column Action
     * 
     * @return string
     */ 
    public static function _rowAction($param = []) {
        return view('admin.helpers.partials.row_action', [
            'btn' => $param
        ])->render(); 
    }

    /**
     * Select Row 
     * 
     * @return string
     */
    public static function _selectRow($data) {
        return view('admin.helpers.partials.select_row', [
            'data' => $data
        ]); 
    }
}