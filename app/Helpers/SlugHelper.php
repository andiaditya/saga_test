<?php

namespace App\Helpers; 

class SlugHelper {

	/**
	 * Generate Slug Category
	 * @return string
	 */
	public static function category($slug) {
		return  url('category/' . $slug); 
	}

	/**
	 * Generate Slug Post
	 * @return string
	 */
	public static function post($slug) {
		return  url('news/' . $slug); 
	}
}