<?php 

namespace App\Models; 

use Illuminate\Database\Eloquent\Model;

class AdminUsersRoles extends Model {

    /**
     * Table Name 
     * 
     * @var string
     */
    protected $table = "admin_users_roles"; 

    /**
     * Primary Key 
     * 
     * @var string
     */
    protected $primaryKey = "id"; 

    public function role() {
        return $this->belongsTo(AdminRoles::class, 'role_id'); 
    }

    /**
     * Fillable 
     * 
     * @var array
     */
    public $fillable = [
        "user_id", 
        "role_id"
    ]; 
    
}