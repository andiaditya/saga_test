<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class AdminUsers extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'avatar', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 
    ];

    /**
     * Relation to table admin_users_roles
     * Many to Many
     */
    public function roles() {
        return $this->hasMany(AdminUsersRoles::class, 'user_id'); 
    }

    /**
     * Relation to table posts
     * One to Many 
     */
    public function posts() {
        return $this->hasMany(Posts::class, 'user_id'); 
    }
}
