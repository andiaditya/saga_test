<?php 

namespace App\Models; 

use Illuminate\Database\Eloquent\Model;

class AdminUsersPermissions extends Model {

    /**
     * Table Name 
     * 
     * @var string
     */
    protected $table = "admin_users_permissions"; 

    /**
     * Primary Key 
     * 
     * @var string
     */
    protected $primaryKey = "id"; 

    /**
     * Fillable 
     * 
     * @var array
     */
    public $fillable = [
        "user_id", 
        "permission_id"
    ]; 
}