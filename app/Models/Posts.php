<?php 

namespace App\Models; 

use Illuminate\Database\Eloquent\Model;

class Posts extends Model {

    /**
     * Table Name 
     * 
     * @var string
     */
    protected $table = "posts"; 

    /**
     * Primary Key 
     * 
     * @var string
     */
    protected $primaryKey = "id"; 

    /**
     * Eloquent to Categories
     * 
     * @return object 
     */
    public function categories() {
        return $this->hasMany(PostsCategories::class, 'post_id'); 
    }

    /**
     * Fillable 
     * 
     * @var array
     */
    public $fillable = [
        "user_id", 
        "title", 
        "slug", 
        "content", 
        "publishdate", 
        "banner"
    ]; 
}