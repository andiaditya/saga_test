<?php 

namespace App\Models; 

use Illuminate\Database\Eloquent\Model;

class AdminRoles extends Model {

    /**
     * Table Name 
     * 
     * @var string
     */
    protected $table = "admin_roles"; 

    /**
     * Primary Key 
     * 
     * @var string
     */
    protected $primaryKey = "id"; 

    public function permissions() {
        return $this->hasMany(AdminRolesPermissions::class, 'role_id'); 
    }

    /**
     * Fillable 
     * 
     * @var array
     */
    public $fillable = [
        "slug", 
        "title"
    ]; 
}