<?php 

namespace App\Models; 

use Illuminate\Database\Eloquent\Model;

class AdminPermissions extends Model {

    /**
     * Table Name 
     * 
     * @var string
     */
    protected $table = "admin_permissions"; 

    /**
     * Primary Key 
     * 
     * @var string
     */
    protected $primaryKey = "id"; 

    /**
     * Fillable 
     * 
     * @var array
     */
    public $fillable = [
        "slug", 
        "title", 
        "http_method",
        "http_path"
    ]; 
}