<?php 

namespace App\Models; 

use Illuminate\Database\Eloquent\Model;

class Categories extends Model {

    /**
     * Table Name 
     * 
     * @var string
     */
    protected $table = "categories"; 

    /**
     * Primary Key 
     * 
     * @var string
     */
    protected $primaryKey = "id"; 

    /**
     * Fillable 
     * 
     * @var array
     */
    public $fillable = [
        "slug", 
        "title"
    ]; 
}