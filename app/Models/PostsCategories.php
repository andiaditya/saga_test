<?php 

namespace App\Models; 

use Illuminate\Database\Eloquent\Model;

class PostsCategories extends Model {

    /**
     * Table Name 
     * 
     * @var string
     */
    protected $table = "posts_categories"; 

    /**
     * Primary Key 
     * 
     * @var string
     */
    protected $primaryKey = "id"; 

    /**
     * Eloquent Category 
     * 
     * @return void
     */
    public function category() {
        return $this->belongsTo(Categories::class, 'category_id'); 
    }

    /**
     * Fillable 
     * 
     * @var array
     */
    public $fillable = [
        "post_id", 
        "category_id", 
    ]; 
}