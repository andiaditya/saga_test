<?php 

namespace App\Models; 

use Illuminate\Database\Eloquent\Model;

class AdminRolesPermissions extends Model {

    /**
     * Table Name 
     * 
     * @var string
     */
    protected $table = "admin_roles_permissions"; 

    /**
     * Primary Key 
     * 
     * @var string
     */
    protected $primaryKey = "id"; 

    public function permission() {
        return $this->belongsTo(AdminPermissions::class, 'permission_id'); 
    }

    /**
     * Fillable 
     * 
     * @var array
     */
    public $fillable = [
        "role_id", 
        "permission_id"
    ]; 
}