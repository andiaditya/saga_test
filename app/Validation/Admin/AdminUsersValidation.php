<?php 

namespace App\Validation; 

class AdminUsersValidation {

    /**
     * Validation Rules
     */
    public function run() {
        request()->validate([
            'name'      => 'required',
            'email'     => 'required', 
            'password'  => 'required',
        ]); 
    }
}