<?php 

namespace App\Validation\Admin; 

class AdminRolesValidation {

    /**
     * Validation Rules
     */
    public function run() {
        request()->validate([
            'slug'      => 'required',
            'title'     => 'required', 
        ]); 
    }
}