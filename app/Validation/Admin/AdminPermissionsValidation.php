<?php 

namespace App\Validation; 

class AdminPermissionsValidation {

    /**
     * Validation Rules
     */
    public function _validate() {
        request()->validate([
            'title'         => 'required',
            'slug'          => 'required', 
            'http_method'   => 'required',
            'http_path'     => 'required'
        ]); 
    }
}