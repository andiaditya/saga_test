<?php 

namespace App\Validation; 

class AdminPostsValidation {

    /**
     * Validation Rules
     */
    public function _validate() {
        request()->validate([
            'title'         => 'required',
            'slug'          => 'required', 
            'content'       => 'required'
        ]); 
    }
}