<?php 

namespace App\Validation; 

class CategoriesValidation {

    /**
     * Validation Rules
     */
    public function _validate() {
        request()->validate([
            'title' => 'required'
        ]); 
    }
}