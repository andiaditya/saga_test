<?php 

namespace App\Repositories\Admin; 

use App\Models\AdminUsers; 
use App\Models\AdminRoles; 
use App\Models\AdminUsersRoles; 
use App\Models\AdminPermissions; 
use App\Models\AdminUsersPermissions; 
use App\Interfaces\Admin\AdminUsersInterface; 

use Hash; 

class AdminUsersRepository implements AdminUsersInterface {
    
    /**
     * Object Model 
     * 
     * @var object
     */
    public $adminUsers;

    /**
     * Construct 
     * 
     * @return void
     */
    public function __construct() {
        $this->adminUsers = new AdminUsers; 
    }

    public function create() {
        $user = $this->adminUsers->create([
            'name'      => request()->name,
            'email'     => request()->email, 
            'avatar'    => '', 
            'password'  => Hash::make(request()->password)
        ]); 

        return $user; 
    }

    public function update() {
        $user = AdminUsers::findOrFail(request()->id);
        $user->name = request()->name; 
        $user->email = request()->email; 
        if ( ! empty(request()->password)) {
            $user->password = Hash::make(request()->password); 
        } 

        return $user; 
    }

    /**
     * JSON Tables 
     * 
     * @return void
     */
    public function jsonTables() {
        return datatables()->of($this->adminUsers->all())
            ->addColumn('select', function($data) {
                return \GridHelper::_selectRow($data); 
            })
            ->addColumn('roles', function($data) {
                $label = ""; 
                foreach ($data->roles as $r) {
                    if ( ! empty($r->role)) {
                        $label .= '<label class="label label-success">'.$r->role->title.'</label> '; 
                    }
                }
                return $label;
            })
            ->addColumn('action', function($data) {
                return \GridHelper::_rowAction([
                    'Edit' => [
                        'btn' => 'btn-default', 
                        'icon' => 'fa-edit', 
                        'route' => route('admin.user.edit', ['id' => $data->id])
                    ], 
                    'Delete'  => [
                        'btn' => 'btn-danger', 
                        'icon' => 'fa-trash-o', 
                        'route' => 'javascript::void()',
                        'custom_attr' => 'onclick=show_delete_button(this) data-id='.$data->id.' data-action='.route('admin.user.delete').''
                    ]
                ]);
            })
            ->rawColumns(['select', 'title', 'roles', 'action'])
            ->toJson(); 
    }
    
    /**
     * Delete
     * 
     * @return void
     */
    public function delete() {
        $id = request()->id; 
        $user = $this->adminUsers->find($id);
        if ( ! empty($user)) {
            $user->delete(); 
            return true; 
        } else {
            return false; 
        }
    }

    public function getDefaultPermission($id) {
        $usersPermission = AdminUsersPermissions::where('user_id', $id);
        $permissions = AdminPermissions::leftJoinSub($usersPermission, 'admin_users_permissions', function($join) {
            $join->on('admin_users_permissions.permission_id', '=', 'admin_permissions.id'); 
        })->select([
            'admin_users_permissions.id as selected', 
            'admin_permissions.id', 
            'admin_permissions.title'
        ]);  
        return $permissions->get(); 
    }

    public function getDefaultRole($id) {
        $usersRole = AdminUsersRoles::where('user_id', $id);
        $roles = AdminRoles::leftJoinSub($usersRole, 'admin_users_roles', function($join) {
            $join->on('admin_users_roles.role_id', '=', 'admin_roles.id'); 
        })->select([
            'admin_users_roles.id as selected', 
            'admin_roles.id', 
            'admin_roles.title'
        ]);  
        return $roles->get(); 
    }

    /**
     * Multiple Delete
     * 
     * @return void
     */
    public function deleteMultiple() {
        $id = request()->id; 
        if (is_array($id)) {
            foreach ($id as $x) {
                $user = $this->adminUsers->find($x);
                if ( ! empty($user)) {
                    $user->delete(); 
                } 
            }

            return true; 
        } else {
            return false; 
        }
    }
}