<?php 

namespace App\Repositories\Admin; 

use App\Models\AdminPermissions; 
use App\Interfaces\Admin\AdminPermissionsInterface; 

class AdminPermissionsRepository implements AdminPermissionsInterface {
    
    /**
     * Object Model 
     * 
     * @var object
     */
    public $adminPermissions;
    
    /**
     * Construct 
     * 
     * @return void
     */
    public function __construct() {
        $this->adminPermissions = new AdminPermissions; 
    }

    /**
     * JSON Tables 
     * 
     * @return void
     */
    public function jsonTables() {
        return datatables()->of($this->adminPermissions->all())
            ->addColumn('select', function($data) {
                return \GridHelper::_selectRow($data); 
            })
            ->addColumn('action', function($data) {
                return \GridHelper::_rowAction([
                    'Edit' => [
                        'btn' => 'btn-default', 
                        'icon' => 'fa-edit', 
                        'route' => route('admin.permission.edit', ['id' => $data->id])
                    ], 
                    'Delete'  => [
                        'btn' => 'btn-danger', 
                        'icon' => 'fa-trash-o', 
                        'route' => 'javascript::void()',
                        'custom_attr' => 'onclick=show_delete_button(this) data-id='.$data->id.' data-action='.route('admin.permission.delete').''
                    ]
                ]);
            })
            ->rawColumns(['select', 'title', 'action'])
            ->toJson(); 
    }

    /**
     * Validation Rules 
     * 
     * @return void
     */
    public function _validate() {
        request()->validate([
            'title' => 'required'
        ]); 
    }

    /**
     * Cacth Input User
     * 
     * @return array
     */
    protected function catchInput() {
        $output = [
            'title' => request()->title, 
            'slug'  => request()->slug, 
            'http_method' => request()->http_method, 
            'http_path' => request()->http_path  
        ];

        return $output; 
    }

    /**
     * Create 
     * 
     * @return void
     */
    public function create() {
        $this->_validate(); 
        $data = $this->catchInput(); 
        $this->adminPermissions->create($data);  
    }

    /**
     * Update 
     * 
     * @return void
     */
    public function update() {
        $this->_validate(); 
        $data = $this->catchInput(); 
        $this->adminPermissions->where('id', request()->id)->update($data); 
    }

    /**
     * Delete
     * 
     * @return void
     */
    public function delete() {
        $id = request()->id; 
        $permission = $this->adminPermissions->find($id);
        if ( ! empty($permission)) {
            $permission->delete(); 
            return true; 
        } else {
            return false; 
        }
    }

    /**
     * Multiple Delete
     * 
     * @return void
     */
    public function deleteMultiple() {
        $id = request()->id; 
        if (is_array($id)) {
            foreach ($id as $x) {
                $permission = $this->adminPermissions->find($x);
                if ( ! empty($permission)) {
                    $permission->delete(); 
                } 
            }

            return true; 
        } else {
            return false; 
        }
    }
}