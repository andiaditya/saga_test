<?php 

namespace App\Repositories\Admin; 

use App\Models\AdminRolesPermissions; 
use App\Interfaces\Admin\AdminRolesPermissionsInterface; 

class AdminRolesPermissionsRepository implements AdminRolesPermissionsInterface {
    
    /**
     * Object Model 
     * 
     * @var object
     */
    public $adminRolesPermissions;
    
    /**
     * Construct 
     * 
     * @return void
     */
    public function __construct() {
        $this->adminRolesPermissions = new AdminRolesPermissions; 
    }

    /**
     * Delete Data By Role 
     * 
     * @return void
     */ 
    public function deleteByRole() {
        $id = request()->id; 
        $data = AdminRolesPermissions::where("role_id", $id)->get(); 
        foreach ($data as $i) {
            $item = AdminRolesPermissions::where("id", $i->id)->delete(); 
        }
    }
}