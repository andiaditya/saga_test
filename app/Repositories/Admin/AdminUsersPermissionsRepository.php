<?php 

namespace App\Repositories\Admin; 

use App\Models\AdminUsersPermissions; 
use App\Interfaces\Admin\AdminUsersPermissionsInterface; 

class AdminUsersPermissionsRepository implements AdminUsersPermissionsInterface {
    
    /**
     * Object Model 
     * 
     * @var object
     */
    public $adminUsersPermissions;
    
    /**
     * Construct 
     * 
     * @return void
     */
    public function __construct() {
        $this->adminUsersPermissions = new AdminUsersPermissions; 
    }

    /**
     * Delete Data By User
     * 
     * @return void
     */ 
    public function deleteByUser() {
        $id = request()->id; 
        $data = AdminUsersPermissions::where("user_id", $id)->get(); 
        foreach ($data as $i) {
            $item = AdminUsersPermissions::where("id", $i->id)->delete(); 
        }
    }
}