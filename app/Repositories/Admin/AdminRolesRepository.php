<?php 

namespace App\Repositories\Admin; 

use App\Models\AdminRoles; 
use App\Models\AdminPermissions; 
use App\Models\AdminRolesPermissions; 
use App\Interfaces\Admin\AdminRolesInterface; 

class AdminRolesRepository implements AdminRolesInterface {
    
    /**
     * Object Model 
     * 
     * @var object
     */
    public $adminRoles;
    
    /**
     * Construct 
     * 
     * @return void
     */
    public function __construct() {
        $this->adminRoles = new AdminRoles; 
    }

    /**
     * JSON Tables 
     * 
     * @return void
     */
    public function jsonTables() {
        return datatables()->of($this->adminRoles->all())
            ->addColumn('select', function($data) {
                return \GridHelper::_selectRow($data); 
            })
            ->addColumn('permissions', function($data) {
                $label = ""; 
                foreach ($data->permissions as $permit) {
                    if ( ! empty($permit->permission)) {
                        $label .= '<label class="label label-success">'.$permit->permission->title.'</label> '; 
                    }
                }
                return $label;
            })
            ->addColumn('action', function($data) {
                return \GridHelper::_rowAction([
                    'Edit' => [
                        'btn' => 'btn-default', 
                        'icon' => 'fa-edit', 
                        'route' => route('admin.role.edit', ['id' => $data->id])
                    ], 
                    'Delete'  => [
                        'btn' => 'btn-danger', 
                        'icon' => 'fa-trash-o', 
                        'route' => 'javascript::void()',
                        'custom_attr' => 'onclick=show_delete_button(this) data-id='.$data->id.' data-action='.route('admin.role.delete').''
                    ]
                ]);
            })
            ->rawColumns(['select', 'title', 'permissions', 'action'])
            ->toJson(); 
    }
    
    /**
     * Cacth Input User
     * 
     * @return array
     */
    protected function catchInput() {
        $output = [
            'title' => request()->title, 
            'slug'  => request()->slug
        ];

        return $output; 
    }

    public function getDefaultPermission($id) {
        $rolesPermission = AdminRolesPermissions::where('role_id', $id);
        $permissions = AdminPermissions::leftJoinSub($rolesPermission, 'admin_roles_permissions', function($join) {
            $join->on('admin_roles_permissions.permission_id', '=', 'admin_permissions.id'); 
        })->select([
            'admin_roles_permissions.id as selected', 
            'admin_permissions.id', 
            'admin_permissions.title'
        ]);  
        return $permissions->get(); 
    }

    /**
     * Create 
     * 
     * @return void
     */
    public function create() {
        $data = $this->catchInput(); 
        $role = $this->adminRoles->create($data);  
        return $role; 
    }

    /**
     * Update 
     * 
     * @return void
     */
    public function update() {
        $data = $this->catchInput(); 
        $role = $this->adminRoles->where('id', request()->id)->update($data); 
        $role = $this->adminRoles->find(request()->id);
        return $role; 
    }

    /**
     * Delete
     * 
     * @return void
     */
    public function delete() {
        $id = request()->id; 
        $role = $this->adminRoles->find($id);
        if ( ! empty($role)) {
            $role->delete(); 
            return true; 
        } else {
            return false; 
        }
    }

    /**
     * Multiple Delete
     * 
     * @return void
     */
    public function deleteMultiple() {
        $id = request()->id; 
        if (is_array($id)) {
            foreach ($id as $x) {
                $role = $this->adminRoles->find($x);
                if ( ! empty($role)) {
                    $role->delete(); 
                } 
            }

            return true; 
        } else {
            return false; 
        }
    }
}