<?php 

namespace App\Repositories\Admin; 

use App\Models\Categories; 
use App\Interfaces\Admin\CategoriesInterface; 

class CategoriesRepository implements CategoriesInterface {
    
    /**
     * Object Model 
     * 
     * @var object
     */
    public $categories;
    
    /**
     * Construct 
     * 
     * @return void
     */
    public function __construct() {
        $this->categories = new Categories; 
    }

    /**
     * Json Select 
     * 
     * @return string 
     */
    public function jsonSelect() {
        $categories = Categories::all(); 
        $html = view('admin.helpers.partials.options', [ 'data' => $categories ])->render();
        return $html;  
    }

    /**
     * JSON Tables 
     * 
     * @return void
     */
    public function jsonTables() {
        return datatables()->of($this->categories->all())
            ->addColumn('select', function($data) {
                return \GridHelper::_selectRow($data); 
            })
            ->editColumn('title', function($data) {
                $title = $data->title; 
                $url = \SlugHelper::category($data->slug); 
                $title .= '<br/><a href="'.$url.'" target="_blank"><i>'.$url.'</i></a>'; 
                return $title; 
            })
            ->addColumn('action', function($data) {
                return \GridHelper::_rowAction([
                    'Edit' => [
                        'btn' => 'btn-default', 
                        'icon' => 'fa-edit', 
                        'route' => route('admin.category.edit', ['id' => $data->id])
                    ], 
                    'Delete'  => [
                        'btn' => 'btn-danger', 
                        'icon' => 'fa-trash-o', 
                        'route' => 'javascript::void()',
                        'custom_attr' => 'onclick=show_delete_button(this) data-id='.$data->id.' data-action='.route('admin.category.delete').''
                    ]
                ]);
            })
            ->rawColumns(['select', 'title', 'action'])
            ->toJson(); 
    }

    /**
     * Validation Rules 
     * 
     * @return void
     */
    public function _validate() {
        request()->validate([
            'title' => 'required'
        ]); 
    }

    /**
     * Cacth Input User
     * 
     * @return array
     */
    protected function catchInput() {
        $output = [
            'title' => request()->title, 
            'slug'  => \Str::slug(request()->title)  
        ];

        return $output; 
    }

    /**
     * Create 
     * 
     * @return void
     */
    public function create() {
        $this->_validate(); 
        $data = $this->catchInput(); 
        $this->categories->create($data);  
    }

    /**
     * Update 
     * 
     * @return void
     */
    public function update() {
        $this->_validate(); 
        $data = $this->catchInput(); 
        $this->categories->where('id', request()->id)->update($data); 
    }

    /**
     * Delete
     * 
     * @return void
     */
    public function delete() {
        $id = request()->id; 
        $category = $this->categories->find($id);
        if ( ! empty($category)) {
            $category->delete(); 
            return true; 
        } else {
            return false; 
        }
    }

    /**
     * Multiple Delete
     * 
     * @return void
     */
    public function deleteMultiple() {
        $id = request()->id; 
        if (is_array($id)) {
            foreach ($id as $x) {
                $category = $this->categories->find($x);
                if ( ! empty($category)) {
                    $category->delete(); 
                } 
            }

            return true; 
        } else {
            return false; 
        }
    }
}