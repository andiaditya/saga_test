<?php 

namespace App\Repositories\Admin; 

use App\Models\Posts; 
use App\Models\Categories; 
use App\Models\PostsCategories; 
use App\Interfaces\Admin\PostsInterface; 
use App\Http\Middleware\Admin; 

use Auth; 

class PostsRepository implements PostsInterface {
    
    /**
     * Object Model 
     * 
     * @var object
     */
    public $posts;
    
    /**
     * Construct 
     * 
     * @return void
     */
    public function __construct() {
        $this->posts = new Posts; 
    }

    /**
     * JSON Tables 
     * 
     * @return void
     */
    public function jsonTables() {
        $admin = new Admin; 
        if ($admin->checkAdmin()) {
            $data = $this->posts->all(); 
        } else {
            $data = $this->posts->where('user_id', Auth::guard('admin')->user()->id); 
        }

        return datatables()->of($data)
            ->addColumn('select', function($data) {
                return \GridHelper::_selectRow($data); 
            })
            ->addColumn('categories', function($data) {
                $label = ""; 
                foreach ($data->categories as $cat) {
                    if ( ! empty($cat->category)) {
                        $label .= '<label class="label label-primary">'.$cat->category->title.'</label> '; 
                    }
                }
                return $label;
            })
            ->editColumn('title', function($data) {
                $title = $data->title; 
                $url = \SlugHelper::post($data->slug); 
                $title .= '<br/><a href="'.$url.'" target="_blank"><i>'.$url.'</i></a>';
                $title .= '<br/><br/>'; 
                $title .= ' <div class="btn-group btn-group-xs">
                                <a class="btn btn-xs btn-default" style="font-size:11px;"><i class="fa fa-user"></i> Super Administrator</a>
                                <a class="btn btn-xs btn-default" style="font-size:11px;"><i class="fa fa-calendar"></i> '.date('d F Y', strtotime($data->publishdate)).'</a>
                            </div>'; 
                return $title; 
            })
            ->addColumn('action', function($data) {
                return \GridHelper::_rowAction([
                    'Edit' => [
                        'btn' => 'btn-default', 
                        'icon' => 'fa-edit', 
                        'route' => route('admin.post.edit', ['id' => $data->id])
                    ], 
                    'Delete'  => [
                        'btn' => 'btn-danger', 
                        'icon' => 'fa-trash-o', 
                        'route' => 'javascript::void()',
                        'custom_attr' => 'onclick=show_delete_button(this) data-id='.$data->id.' data-action='.route('admin.post.delete').''
                    ]
                ]);
            })
            ->rawColumns(['select', 'categories', 'title', 'action'])
            ->toJson(); 
    }

    /**
     * Validation Rules 
     * 
     * @return void
     */
    public function _validate() {
        request()->validate([
            'title' => 'required',
            'content' => 'required'
        ]); 
    }

    public function _upload() {
        if (request()->hasFile('banner')) {

            $image       = request()->file('banner');
            
            $filename    = $image->getClientOriginalName();

            $image_resize = \Image::make($image->getRealPath());              
            $image_resize->resize(550, 370);
            $image_resize->save(public_path('uploads/posts/medium/' .$filename));

            $image->move('uploads/posts/',$image->getClientOriginalName());
            
            return $filename; 
        }
    }

    /**
     * Cacth Input User
     * 
     * @return array
     */
    protected function catchInput() {
        $output = [
            'user_id'       => \Auth::guard('admin')->user()->id, 
            'title'         => request()->title, 
            'slug'          => request()->slug, 
            'banner'        => $this->_upload(), 
            'content'       => request()->content,
            'publishdate'   => request()->publishdate
        ];

        return $output; 
    }

    public function getDefaultCategories($id) {
        $postsCategories = PostsCategories::where('post_id', $id);
        $categories = Categories::leftJoinSub($postsCategories, 'posts_categories', function($join) {
            $join->on('posts_categories.category_id', '=', 'categories.id'); 
        })->select([
            'posts_categories.id as selected', 
            'categories.id', 
            'categories.title'
        ]);  
        return $categories->get(); 
    }

    /**
     * Create 
     * 
     * @return void
     */
    public function create() {
        $this->_validate(); 
        $data = $this->catchInput(); 
        $post = $this->posts->create($data);  
        // Create Category 
        foreach (request()->categories as $category_id) {
            $postCategory = new PostsCategories; 
            $postCategory->post_id = $post->id; 
            $postCategory->category_id = $category_id; 
            $postCategory->save(); 
        }
    }

    /**
     * Update 
     * 
     * @return void
     */
    public function update() {
        $this->_validate(); 
        $id = request()->id; 
        $post = Posts::find($id); 
        if ( ! empty($post)) {
            $post->title = request()->title; 
            $post->slug = request()->slug; 
            $post->content = request()->content; 
            if (request()->hasFile('banner')) {
                $post->banner = $this->_upload(); 
            }
            $post->publishdate = request()->publishdate; 
            $post->save(); 

            PostsCategories::where('post_id', $post->id)->delete(); 

            foreach (request()->categories as $category) {
                PostsCategories::create([
                    'post_id' => $post->id, 
                    'category_id' => $category
                ]); 
            }
        }
    }

    /**
     * Delete
     * 
     * @return void
     */
    public function delete() {
        $id = request()->id; 
        $category = $this->posts->find($id);
        if ( ! empty($category)) {
            $category->delete(); 
            return true; 
        } else {
            return false; 
        }
    }

    /**
     * Multiple Delete
     * 
     * @return void
     */
    public function deleteMultiple() {
        $id = request()->id; 
        if (is_array($id)) {
            foreach ($id as $x) {
                $category = $this->posts->find($x);
                if ( ! empty($category)) {
                    $category->delete(); 
                } 
            }

            return true; 
        } else {
            return false; 
        }
    }
}