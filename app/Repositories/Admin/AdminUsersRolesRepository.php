<?php 

namespace App\Repositories\Admin; 

use App\Models\AdminUsersRoles; 
use App\Interfaces\Admin\AdminUsersRolesInterface; 

class AdminUsersRolesRepository implements AdminUsersRolesInterface {
    
    /**
     * Object Model 
     * 
     * @var object
     */
    public $adminUsersRoles;
    
    /**
     * Construct 
     * 
     * @return void
     */
    public function __construct() {
        $this->adminUsersRoles = new AdminUsersRoles; 
    }

    /**
     * Delete Data By User
     * 
     * @return void
     */ 
    public function deleteByUser() {
        $id = request()->id; 
        $data = AdminUsersRoles::where("user_id", $id)->get(); 
        foreach ($data as $i) {
            $item = AdminUsersRoles::where("id", $i->id)->delete(); 
        }
    }
}