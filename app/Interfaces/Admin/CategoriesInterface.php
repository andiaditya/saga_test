<?php 

namespace App\Interfaces\Admin; 

Interface CategoriesInterface {

    public function create(); 

    public function jsonTables(); 
}