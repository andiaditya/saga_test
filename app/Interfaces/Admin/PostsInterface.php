<?php 

namespace App\Interfaces\Admin; 

Interface PostsInterface {

    public function create(); 

    public function jsonTables(); 
}